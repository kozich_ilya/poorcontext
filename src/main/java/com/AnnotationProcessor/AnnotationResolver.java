package com.AnnotationProcessor;

import com.AnnotationProcessor.CustomAnnotations.*;
import com.AnnotationProcessor.Helpers.*;
import com.AnnotationProcessor.Wrappers.*;

import java.lang.annotation.*;
import java.lang.reflect.*;
import java.util.*;

import org.slf4j.*;

public class AnnotationResolver {
    private final static Logger LOGGER = LoggerFactory.getLogger(AnnotationResolver.class.getCanonicalName());

    private AnnotationResolver() { }

    @SafeVarargs
    public static boolean checkForClassAnnotations(Class<?> objectClass, Class<? extends Annotation> ...annotations) {
        if(annotations == null) throw new IllegalArgumentException();

        for (Class<? extends Annotation> annotation : annotations) {
            if(!objectClass.isAnnotationPresent(annotation))
                return false;
        }

        return true;
    }

    public static boolean checkForClassAnnotation(Class<?> objectClass, Class<? extends Annotation> ...annotations) {
        if(annotations == null) throw new IllegalArgumentException();

        for (Class<? extends Annotation> annotation : annotations) {
            if(objectClass.isAnnotationPresent(annotation))
                return true;
        }

        return false;
    }

    @SafeVarargs
    public static boolean checkForMethodAnnotation(Class<?> objectClass, Class<? extends Annotation> ...annotations) {
        if(annotations == null) throw new IllegalArgumentException();

        for (Method method : objectClass.getDeclaredMethods()) {
            for (Class<? extends Annotation> annnotation: annotations) {
                if(!method.isAnnotationPresent(annnotation)) {
                    return false;
                }
            }
        }

        return true;
    }

    public static boolean checkForMethodAnnotationAndSignature(Class<?> objectClass, String methodName,
                                                               Class<?>[] methodParameters,
                                                               Class<? extends Annotation> annotations) {
        if(!checkForMethodAnnotation(objectClass, annotations)
                || methodParameters == null) {
            throw new IllegalArgumentException();
        }

        for (Method method : objectClass.getDeclaredMethods()) {
            if(method.getName().equals(methodName)
                    && Arrays.equals(method.getParameterTypes(), methodParameters)) {
                return true;
            }
        }

        return false;
    }

    public static Map<String, Class<?>> getExistingClassesByKey(Map<String, String> propertiesMap) {
        Map<String,Class<?>> existingClassesByKey = new HashMap<>();

        for (Map.Entry<String, String> entry : propertiesMap.entrySet()) {
            try {
                existingClassesByKey.put(entry.getKey(), Class.forName(entry.getValue()));
            } catch (ClassNotFoundException classNotFoundException) {
                LOGGER.info("Property file contains invalid path to class.", classNotFoundException);
            }
        }

        return existingClassesByKey;
    }

    @SafeVarargs
    public static KeyBeanMap getClassesByKey(PropertiesWrapper propertiesWrapper, Class<? extends Annotation>... annotationClasses) {
        KeyBeanMap keyBeanMap = new KeyBeanMap();
        Map<String, Class<?>> existingClassesByKey = getExistingClassesByKey(propertiesWrapper.getPropertiesMap());

        existingClassesByKey.forEach((key, value) -> {
                keyBeanMap.put(key, value, null);
            });

        return keyBeanMap;
    }

    public static Map<String, String> getClassNamesWithAnnotationsByKey(Map<String, Class<?>> existingClassesByKey) {
        Map<String, String> classNamesWithAnnotationByKey = new HashMap<>();

        existingClassesByKey.forEach((key, value) -> {
            if(checkForMethodAnnotation(value, MethodAnnotation.class)
                    || checkForClassAnnotations(value, ClassAnnotation.class)) {
                String formattedValue = StringFormatter.formatClassNameWithAnnotations(value);
                classNamesWithAnnotationByKey.put(key, formattedValue);
            }
        });

        return classNamesWithAnnotationByKey;
    }

    private static Class<? extends Annotation>[] getCustomAnnotations(Class<?> objectClass) {
        Class<? extends Annotation>[] annotations = new Class[objectClass.getAnnotations().length];
        int index = 0;

        for (Annotation annotation : objectClass.getAnnotations()) {
            annotations[index] = annotation.annotationType();
            index++;
        }

        return annotations;
    }
}