package com.AnnotationProcessor;

import com.AnnotationProcessor.CustomAnnotations.*;
import com.AnnotationProcessor.Helpers.*;
import com.AnnotationProcessor.Wrappers.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;

public class PoorContext {
    private final KeyBeanMap KEY_BEAN_MAP;

    private final Map<Class<?>, Class<?>> PRIMITIVES_WRAPPER_MAP;

    private final Logger LOGGER = LoggerFactory.getLogger(PoorContext.class.getCanonicalName());

    {
        PRIMITIVES_WRAPPER_MAP = new HashMap<>();
        PRIMITIVES_WRAPPER_MAP.put(Boolean.class, boolean.class);
        PRIMITIVES_WRAPPER_MAP.put(Byte.class, byte.class);
        PRIMITIVES_WRAPPER_MAP.put(Character.class, char.class);
        PRIMITIVES_WRAPPER_MAP.put(Double.class, double.class);
        PRIMITIVES_WRAPPER_MAP.put(Float.class, float.class);
        PRIMITIVES_WRAPPER_MAP.put(Integer.class, int.class);
        PRIMITIVES_WRAPPER_MAP.put(Long.class, long.class);
        PRIMITIVES_WRAPPER_MAP.put(Short.class, short.class);
    }

    public PoorContext(InputStream inputStream) {
        PropertiesWrapper propertiesWrapper = PropertiesReader.getPropertiesWrapperFromFile(inputStream);
        KEY_BEAN_MAP = AnnotationResolver.getClassesByKey(propertiesWrapper, PoorComponent.class);
    }

    @SuppressWarnings("unchecked")
    public <T> T getPoorBean(String propertyKey) {
        BeanModel bean = KEY_BEAN_MAP.get(propertyKey);

        if (bean == null) throw new IllegalArgumentException("There's no classes with this key.");

        PoorComponent poorComponent = getBeanAnnotation(PoorComponent.class, bean);

        try {
            if (poorComponent.scope() == ScopeType.PROTOTYPE) {
                bean.setInstance(bean.getObjectClass().newInstance());
            }
            else if (poorComponent.scope() == ScopeType.SINGLETON && !bean.isInstantieted()) {
                bean.setInstance(bean.getObjectClass().newInstance());
            }

            autowireFields(bean.getInstance(), bean.getObjectClass());

            return (T)bean.getInstance();
        } catch (InstantiationException | IllegalAccessException exception) {
            LOGGER.error("Cant access to the constructor or class is abstract or it's interface", exception);
        }

        assert false: "You won't be here...";
        return null;
    }

    @SuppressWarnings("unchecked")
    public <T> T getPoorBean(String propertyKey, Object... parameters) {
        BeanModel bean = KEY_BEAN_MAP.get(propertyKey);

        if (bean == null) throw new IllegalArgumentException("There's no classes with this key.");

        PoorComponent poorComponent = getBeanAnnotation(PoorComponent.class, bean);

        if (poorComponent.scope() == ScopeType.PROTOTYPE) {
            Class<?>[] parametersTypes = getParametersTypes(parameters);
            bean.setInstance(createNewInstance(bean.getObjectClass(), parametersTypes, parameters));
        }
        else if (poorComponent.scope() == ScopeType.SINGLETON && !bean.isInstantieted()) {
            Class<?>[] parametersTypes = getParametersTypes(parameters);
            bean.setInstance(createNewInstance(bean.getObjectClass(), parametersTypes, parameters));
        }

        autowireFields(bean.getInstance(), bean.getClass());

        return (T)bean.getInstance();
    }

    private <T> T getBeanAnnotation(Class<? extends Annotation> annotation, BeanModel bean) {
        T poorComponent = null;

        poorComponent = bean.getAnnotation(annotation);

        if(poorComponent != null) {
            return poorComponent;
        }
        else {
            throw new IllegalArgumentException("No such annotation");
        }
    }

    private Object createNewInstance(Class<?> objectClass, Class<?>[] parametersTypes, Object... parameters) {
        try {
            return objectClass.getConstructor(parametersTypes).newInstance(parameters);
        } catch (Exception exception) {
            LOGGER.error("Cant access to the constructor or class is abstract or it's interface", exception);
        }

        throw new IllegalArgumentException();
    }

    private Class<?>[] getParametersTypes(Object ...parameters) {
        Class<?>[] parametersTypes = new Class<?>[parameters.length];
        int index = 0;

        for (Object parameter: parameters) {
            if (PRIMITIVES_WRAPPER_MAP.get(parameter.getClass()) != null) {
                parametersTypes[index] = PRIMITIVES_WRAPPER_MAP.get(parameter.getClass());
            }
            else {
                parametersTypes[index] = parameter.getClass();
            }
            index++;
        }

        return parametersTypes;
    }

    private void autowireFields(Object instance, Class<?> instanceClass) {
        Field[] fields = instanceClass.getDeclaredFields();

        try {
            for (Field field : fields) {
                if (field.isAnnotationPresent(Autowired.class)) {
                    field.setAccessible(true);
                    Class<?> fieldType = field.getType();
                    field.set(instance,
                            getPoorBean(KEY_BEAN_MAP.getKeyOfBean(fieldType))
                    );
                }
            }
        } catch (IllegalAccessException illegalAccessException) {
            LOGGER.error("Cant access to the constructor.", illegalAccessException);
        }
    }
}