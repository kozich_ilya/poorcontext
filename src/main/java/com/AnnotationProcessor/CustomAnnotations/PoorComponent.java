package com.AnnotationProcessor.CustomAnnotations;

import com.AnnotationProcessor.Helpers.*;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PoorComponent {
    ScopeType scope () default ScopeType.SINGLETON;
}
