package com.AnnotationProcessor;

import static com.AnnotationProcessor.AnnotationResolver.*;

import com.AnnotationProcessor.Helpers.*;
import com.AnnotationProcessor.Wrappers.*;

import java.io.*;
import java.lang.annotation.*;
import java.util.*;

public interface AnnotationResolverAPI {
    static boolean checkForClassAnnotation(Class<?> objectClass, Class<? extends Annotation> annotation) {
        return AnnotationResolver.checkForClassAnnotations(objectClass, annotation);
    }

    static boolean checkForMethodAnnotation(Class<?> objectClass, Class<? extends  Annotation> annotation) {
        return AnnotationResolver.checkForMethodAnnotation(objectClass, annotation);
    }

    static boolean checkForMethodAnnotationAndSignature(
            Class<?> objectClass, Class<? extends Annotation> annotation, String methodName, Class<?>[] methodParams
    ) {
        return AnnotationResolver.checkForMethodAnnotationAndSignature(objectClass, methodName, methodParams, annotation);
    }

    static Map<String, String> processProperties(InputStream inputStream) {
        PropertiesWrapper propertiesWrapper = PropertiesReader.getPropertiesWrapperFromFile(inputStream);
        Map<String, Class<?>> existingClassesByKey = getExistingClassesByKey(propertiesWrapper.getPropertiesMap());

        return AnnotationResolver.getClassNamesWithAnnotationsByKey(existingClassesByKey);
    }
}
