package com.AnnotationProcessor.Helpers;

import com.AnnotationProcessor.CustomAnnotations.*;

import java.lang.reflect.*;

public class StringFormatter {
    private StringFormatter() { }

    public static String formatClassNameWithAnnotations(Class<?> objectClass) {
        return getClassNameWithAnnotationName(objectClass) + getMethodNameWithAnnotation(objectClass);
    }

    private static String getMethodNameWithAnnotation(Class<?> objectClass) {
        if(objectClass == null) throw new IllegalArgumentException();

        StringBuilder formattedMethodNameWithAnnotation;

        Method[] methods = objectClass.getDeclaredMethods();
        if(methods.length <= 0) {
            return "";
        }
        else {
            formattedMethodNameWithAnnotation = new StringBuilder();
        }

        for (Method method: methods) {
            if(method.isAnnotationPresent(MethodAnnotation.class)) {
                formattedMethodNameWithAnnotation.append("Method - ")
                        .append(method.getName()).append(" with annotation - ")
                        .append(MethodAnnotation.class.getSimpleName()).append(". ");
            }
        }

        return formattedMethodNameWithAnnotation.toString();
    }

    private static String getClassNameWithAnnotationName(Class<?> objectClass) {
        if(objectClass == null) throw new IllegalArgumentException();

        if(objectClass.isAnnotationPresent(ClassAnnotation.class)) {
            StringBuilder formattedClassNameWithAnnotation = new StringBuilder();
            return formattedClassNameWithAnnotation.append("Class - ")
                    .append(objectClass.getSimpleName()).append(" with annotation - ")
                    .append(ClassAnnotation.class.getSimpleName()).append(". ").toString();
        }
        else {
            return "";
        }
    }
}
