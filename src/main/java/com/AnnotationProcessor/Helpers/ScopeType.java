package com.AnnotationProcessor.Helpers;

public enum ScopeType {
    PROTOTYPE, SINGLETON;
}
