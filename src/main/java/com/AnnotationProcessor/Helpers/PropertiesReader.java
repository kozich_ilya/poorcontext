package com.AnnotationProcessor.Helpers;

import com.AnnotationProcessor.Wrappers.PropertiesWrapper;

import java.io.*;
import java.util.*;

public class PropertiesReader {
    private PropertiesReader() { }

    public static Properties getPropertiesFromFile(InputStream inputStream) {
        Properties properties;
        try{
            properties = new Properties();
            properties.load(inputStream);
        } catch(IOException ioException) {
            System.out.println(ioException.getMessage());
            throw new IllegalArgumentException(ioException);
        }

        return properties;
    }

    public static PropertiesWrapper getPropertiesWrapperFromFile(InputStream inputStream) {
        return new PropertiesWrapper(getPropertiesFromFile(inputStream));
    }
}
