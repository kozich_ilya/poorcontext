package com.AnnotationProcessor.Helpers;

public class Validator {
    private Validator() { }

    public static boolean isClassExist(String classPath) {
        try {
            Class.forName(classPath);
        } catch (ClassNotFoundException classNotFoundException) {
            return false;
        }

        return true;
    }
}
