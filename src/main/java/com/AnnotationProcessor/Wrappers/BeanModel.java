package com.AnnotationProcessor.Wrappers;

import java.lang.annotation.*;

public class BeanModel {
    Class<?> objectClass;
    Object instance;

    public BeanModel(Class<?> objectClass, Object instance) {
        if(objectClass == null) throw new IllegalArgumentException();

        this.objectClass = objectClass;
        this.instance = instance;
    }

    public <T> T getAnnotation(Class<? extends Annotation> annotation) {
        return (T)objectClass.getAnnotation(annotation);
    }

    public boolean isInstantieted() {
        return instance != null;
    }

    public void setInstance(Object newInstance) {
        instance = newInstance;
    }

    public Object getInstance() {
        return instance;
    }

    public Class<?> getObjectClass() {
        return objectClass;
    }
}
