package com.AnnotationProcessor.Wrappers;

import java.util.*;

public class PropertiesWrapper {
    private final Properties PROPERTIES;

    public PropertiesWrapper(Properties properties) {
        PROPERTIES = properties;
    }

    public Map<String, String> getPropertiesMap() {
        HashMap<String, String> hashMap = new HashMap<>();

        PROPERTIES.forEach((key, value) -> hashMap.put((String) key, (String) value));

        return hashMap;
    }
}
