package com.AnnotationProcessor.Wrappers;

import java.lang.annotation.*;
import java.util.*;

public class KeyBeanMap {
    private final Map<String, BeanModel> KEY_BEAN_MAP;

    public KeyBeanMap() {
        KEY_BEAN_MAP = new HashMap<>();
    }

    public BeanModel put(String key, Class<?> objectClass, Object instance) {
        return KEY_BEAN_MAP.put(key, new BeanModel(objectClass, instance));
    }

    public BeanModel get(String key) {
        return KEY_BEAN_MAP.get(key);
    }

    public Set<Map.Entry<String, BeanModel>> entrySet() {
        return KEY_BEAN_MAP.entrySet();
    }

    public String getKeyOfBean(Class<?> objectClass) {
        for (Map.Entry<String, BeanModel> entry : KEY_BEAN_MAP.entrySet()) {
            if(entry.getValue().getObjectClass() == objectClass)
                return entry.getKey();
        }

        throw new IllegalArgumentException("No such bean declared");
    }
}
