package com;

import com.AnnotationProcessor.CustomAnnotations.*;
import com.AnnotationProcessor.*;
import com.TestClasses.*;

import java.io.*;

import org.slf4j.*;

@groovy.util.logging.Slf4j
public class Main {
    static final String shortPath = "src\\main\\resources\\pathsToClasses.properties";

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class.getCanonicalName());

    static PoorContext poorContext;

    static {
        try (InputStream inputStream = new FileInputStream(shortPath)) {
            poorContext = new PoorContext(inputStream);
        } catch (IOException ioException) {
            LOGGER.error("Wrong path to property file.", ioException);
        }
    }

    public static void main(String[] args) {
        testPoorContextWithoutParameters();
    }

    static void testProperties() {
       /* Map<String, String> map = AnnotationResolverAPI.processProperties(inputStream);

        map.forEach((key, value) ->
                System.out.println("Key: " + key + ", value: " + value)
        );*/
    }

    static void testCheckForAnnotations() {
        Class<ClassAnnotation> classAnnotation = ClassAnnotation.class;

        System.out.println(
                AnnotationResolverAPI.checkForClassAnnotation(TestClass1.class, classAnnotation)
                        ? "Annotation " + classAnnotation.getSimpleName() + " present!"
                        : "Annotation " + classAnnotation.getSimpleName() + " doesn't present!"
        );

        Class<MethodAnnotation> methodAnnotation = MethodAnnotation.class;

        System.out.println(
                AnnotationResolverAPI.checkForMethodAnnotation(TestClass1.class, methodAnnotation)
                        ? "Annotation " + methodAnnotation.getSimpleName() + " present!"
                        : "Annotation " + methodAnnotation.getSimpleName() + " doesn't present!"
        );
    }

    static void testForAnnotationsAndSignature() {
        Class<MethodAnnotation> methodAnnotation = MethodAnnotation.class;
        String methodName = "exampleMethod";
        Class<?>[] methodParameters = { int.class, short.class, TestClass1.class };

        System.out.println(
                AnnotationResolverAPI.checkForMethodAnnotationAndSignature(TestClass1.class, methodAnnotation, methodName, methodParameters)
                        ? "Annotation " + methodAnnotation.getSimpleName() + " present and signature is the same!"
                        : "Annotation" + methodAnnotation.getSimpleName() + " doesn't present or signature is different!"
        );
    }

    static void testPoorContextWithoutParameters() {
        TestClass1 tc11 = poorContext.getPoorBean("first");
        TestClass1 tc12 = poorContext.getPoorBean("first");
        TestClass4 tc41 = poorContext.getPoorBean("fourth");
        TestClass5 tc51 = poorContext.getPoorBean("fifth");

        System.out.println(tc11);
        System.out.println(tc12);
        System.out.println(tc41);
        tc11.print();
    }

    static void testPoorContextWithParameters() {
        TestClass3 tc3 = poorContext.getPoorBean("third", "some string", 666);

        System.out.println("TestClass 3 values:" + " string = " + tc3._text + ", int = " + tc3._value);
    }
}
