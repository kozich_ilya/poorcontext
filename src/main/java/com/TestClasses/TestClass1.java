package com.TestClasses;

import com.AnnotationProcessor.CustomAnnotations.*;
import com.AnnotationProcessor.Helpers.*;

@ClassAnnotation
@PoorComponent(scope = ScopeType.PROTOTYPE)
public class TestClass1 {
    @Autowired
    private TestClass4 tc4;
    public TestClass1() { }
    @MethodAnnotation
    void exampleMethod(int i, short s, TestClass1 ec) { }

    public void print() {
        System.out.println("TestClass 1");
        tc4.print();
    }
}
