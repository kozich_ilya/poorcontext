package com.TestClasses;


import com.AnnotationProcessor.CustomAnnotations.*;

public class TestClass2 {
    @MethodAnnotation
    private boolean testClassPrivateMethod() {
        return true;
    }

    @MethodAnnotation
    public boolean testClassPublicMethod() {
        return true;
    }
}
