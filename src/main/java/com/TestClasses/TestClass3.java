package com.TestClasses;

import com.AnnotationProcessor.CustomAnnotations.*;

@ClassAnnotation
@PoorComponent
public class TestClass3 {
    public String _text;
    public int _value;
    public TestClass3(String text, int value) {
        _text = text;
        _value = value;
    }
}
